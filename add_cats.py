from app import app, db
from models import Cat

with app.app_context():
    # Удалим все записи из таблицы и создадим заново
    db.drop_all()
    db.create_all()

    # Добавление тестовых данных
    cats = [
        Cat(name="Bella", breed="Persian", description="A calm Persian cat", age_months=24, image_url="https://example.com/persian.jpg"),
        Cat(name="Buddy", breed="Norwegian Forest", description="A strong Norwegian Forest cat", age_months=27, image_url="https://example.com/norwegian.jpg"),
        Cat(name="Charlie", breed="Abyssinian", description="A playful Abyssinian cat", age_months=20, image_url="https://example.com/abyssinian.jpg"),
        Cat(name="Chloe", breed="Scottish Fold", description="A calm Scottish Fold cat", age_months=28, image_url="https://example.com/scottish_fold.jpg"),
        Cat(name="Daisy", breed="Turkish Angora", description="An elegant Turkish Angora cat", age_months=19, image_url="https://example.com/turkish_angora.jpg"),
        Cat(name="Gizmo", breed="Himalayan", description="A fluffy Himalayan cat", age_months=26, image_url="https://example.com/himalayan.jpg"),
        Cat(name="Jasper", breed="Selkirk Rex", description="A cute Selkirk Rex cat", age_months=16, image_url="https://example.com/selkirk_rex.jpg"),
        Cat(name="Leo", breed="Bengal", description="An energetic Bengal cat", age_months=18, image_url="https://example.com/bengal.jpg"),
        Cat(name="Loki", breed="Devon Rex", description="A curious Devon Rex cat", age_months=17, image_url="https://example.com/devon_rex.jpg"),
        Cat(name="Max", breed="Maine Coon", description="A large Maine Coon cat", age_months=36, image_url="https://example.com/maine_coon.jpg"),
        Cat(name="Nala", breed="Sphynx", description="A hairless Sphynx cat", age_months=30, image_url="https://example.com/sphynx.jpg"),
        Cat(name="Oliver", breed="Russian Blue", description="A quiet Russian Blue cat", age_months=15, image_url="https://example.com/russian_blue.jpg"),
        Cat(name="Oscar", breed="Birman", description="A calm Birman cat", age_months=29, image_url="https://example.com/birman.jpg"),
        Cat(name="Rusty", breed="Chartreux", description="A playful Chartreux cat", age_months=24, image_url="https://example.com/chartreux.jpg"),
        Cat(name="Sammy", breed="Exotic Shorthair", description="A cute Exotic Shorthair cat", age_months=21, image_url="https://example.com/exotic_shorthair.jpg"),
        Cat(name="Simba", breed="Ragdoll", description="An affectionate Ragdoll cat", age_months=22, image_url="https://example.com/ragdoll.jpg"),
        Cat(name="Toby", breed="Oriental", description="An active Oriental cat", age_months=23, image_url="https://example.com/oriental.jpg")
    ]

    db.session.bulk_save_objects(cats)
    db.session.commit()
