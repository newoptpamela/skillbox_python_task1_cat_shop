
from flask import Blueprint, render_template, request
from models import Cat
from sqlalchemy import or_

main_blueprint = Blueprint('main', __name__)


@main_blueprint.route('/')
def index():
    page = request.args.get('page', 1, type=int)
    search = request.args.get('search', '')
    sort_by = request.args.get('sort_by', 'name')

    query = Cat.query.filter(
        or_(
            Cat.breed.ilike(f'%{search}%'),
            Cat.name.ilike(f'%{search}%'),
            Cat.description.ilike(f'%{search}%')
        )
    )

    if sort_by == 'breed':
        query = query.order_by(Cat.breed)
    elif sort_by == 'relevance':
        # Пример сортировки по релевантности, можно использовать текстовое совпадение
        query = query.order_by(Cat.name.contains(search) + Cat.description.contains(search))
    elif sort_by == 'age':
        query = query.order_by(Cat.age_months)
    else:
        query = query.order_by(Cat.name)

    cats = query.paginate(page=page, per_page=5, error_out=False)
    return render_template('index.html', cats=cats.items,
                           next_url=f"/?page={cats.next_num}&search={search}&sort_by={sort_by}",
                           prev_url=f"/?page={cats.prev_num}&search={search}&sort_by={sort_by}")


@main_blueprint.route('/cat/<int:cat_id>')
def cat_detail(cat_id):
    cat = Cat.query.get_or_404(cat_id)
    return render_template('cat_detail.html', cat=cat)
