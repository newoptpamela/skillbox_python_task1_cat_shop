# Онлайн-магазин породистых котят

## Задание
Задача: спроектируйте API для онлайн-магазин породистых котят. Используемые технологии: Flask, sqlalchemy + postgres, docker.

Создайте простой портал для электронной библиотеки. Админка должна содержать две страницы:
1. список всех котиков с пагинацией (по 5 котов) и поиском. При нажатии на котика попадаем на вторую страницу
2. страница отдельного котика (на ней должен отображаться порода, картинка, имя, краткое описание и возраст в месяцах)

Должен поддерживаться полнотекстовый поиск по всем полям информации о котиках (порода, имя, возраст, описание). Должна быть возможность выбрать сортировку отдаемого результата: по породе, по возрасту, по релевантности (сортировка по умолчанию)

Решение оформить в виде проекта на Flask в обвязке Docker контейнера. Запуск решения должен происходить по команде "docker-compose up". В магазине должно быть как минимум 20 котиков для проверки реализации.

Визуально проект не должен выглядеть отталкивающе. Мы любим котиков :-)

## Описание проекта
Этот проект представляет собой онлайн-магазин породистых котят и включает:
1. Список всех котят с пагинацией и поиском.
2. Страница с детальной информацией о котенке.

## Используемые технологии
- Flask
- SQLAlchemy
- PostgreSQL
- Docker

## Структура проекта


## Project Structure
Skillbox_Python_task1_cat_shop/
```plaintext
├── app.py                 # Main application file
├── add_cats.py            # Script to populate the database with initial data
├── models.py              # Database models
├── routes.py              # Application routes
├── Dockerfile             # Docker configuration
├── docker-compose.yml     # Docker Compose configuration
├── requirements.txt       # Python dependencies
├── static
│   └── styles.css         # CSS file for styling
└── templates
    ├── base.html          # Base HTML template
    ├── index.html         # Template for listing kittens
    └── cat_detail.html    # Template for detailed view of a kitten
```

## Настройка и установка
1. Клонировать репозиторий:
    ```bash
    git clone <URL репозитория>
    cd <название папки репозитория>
    ```
2. Создать и активировать виртуальное окружение:
    ```bash
    python -m venv venv
    source venv/bin/activate
    ```
3. Установить зависимости:
    ```bash
    pip install -r requirements.txt
    ```
4. Запустить проект в Docker:
    ```bash
    docker-compose up
    ```

5. Добавить тестовые данные:
    ```bash
    docker-compose exec web bash
    python add_cats.py
    exit
    ```

## Использование

### Основные URL для проверки

Ниже представлены основные функции вашего веб-приложения для управления каталогом котов, доступные через определенные URL. Каждая функция включает в себя прямые ссылки для проверки и примеры, которые помогут вам лучше понять, как взаимодействовать с приложением.

#### Главная страница
- **URL:** [http://127.0.0.1:5001/](http://127.0.0.1:5001/)
- **Описание:** На главной странице отображается список всех котов, зарегистрированных в системе. Список включает имя кота, породу и возраст.
- **Пример использования:** Просто перейдите по ссылке для просмотра всех котов.

#### Поиск котов
- **URL для поиска:** [http://127.0.0.1:5001/?search=Siamese](http://127.0.0.1:5001/?search=Siamese)
- **Описание:** Поиск позволяет фильтровать котов по имени, породе или описанию. Используйте параметр `search` для указания ключевого слова.
- **Пример использования:** Для поиска котов породы "Сиамский" используйте параметр `search=Siamese`. Вы можете изменить ключевое слово на любое другое для проверки других записей.

#### Сортировка котов

- **Описание:** Используйте следующие URL для сортировки котов по различным критериям. Выберите критерий сортировки, указав соответствующее значение параметра `sort_by`.

- **URL и описания:**
  - **По имени:**
    - URL: `http://127.0.0.1:5001/?sort_by=name`
    - [Сортировать по имени](http://127.0.0.1:5001/?sort_by=name)
  - **По породе:**
    - URL: `http://127.0.0.1:5001/?sort_by=breed`
    - [Сортировать по породе](http://127.0.0.1:5001/?sort_by=breed)
  - **По возрасту:**
    - URL: `http://127.0.0.1:5001/?sort_by=age`
    - [Сортировать по возрасту](http://127.0.0.1:5001/?sort_by=age)
  - **По релевантности:**
    - URL: `http://127.0.0.1:5001/?sort_by=relevance`
    - [Сортировать по релевантности](http://127.0.0.1:5001/?sort_by=relevance)

#### Пагинация
- **URL:** [http://127.0.0.1:5001/?page=2](http://127.0.0.1:5001/?page=2)
- **Описание:** Для удобства просмотра больших списков котов, список делится на страницы. Используйте параметр `page` для навигации по страницам.
- **Пример использования:** Для просмотра второй страницы списка котов используйте URL выше. Измените номер страницы в URL, чтобы перейти к другим страницам.

#### Страница детального просмотра кота
- **URL:** [http://127.0.0.1:5001/cat/1](http://127.0.0.1:5001/cat/1)
- **Описание:** Каждый кот имеет свою страницу с детальной информацией, включая фотографии, возраст, породу и описание.
- **Пример использования:** Чтобы узнать больше о коте с идентификатором 1, перейдите по данной ссылке. Измените `cat_id` в URL, чтобы просмотреть информацию о других котах.

## Почему задание выполнено
- Реализованы страницы со списком котов и детальной информацией о коте.
- Поддерживается пагинация, поиск и сортировка.
- Проект успешно запускается в Docker и содержит 20 котов для проверки.

