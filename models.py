from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Cat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    breed = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(200))
    age_months = db.Column(db.Integer)
    image_url = db.Column(db.String(200), nullable=False)
